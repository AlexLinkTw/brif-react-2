function App() {

  const [countFestival, setCountFestival] = useState(0)
  const [countMatch, setCountMatch] = useState(0)


  return (

    <div className="App">


      <div className="Button__Festival">
      <h2>Festival count</h2>
      <button onClick={() => setCountFestival((countFestival) => countFestival + 1)}>
        increase count +
      </button>
      <h2>{countFestival}</h2>
      <button onClick={() => setCountFestival((countFestival) => countFestival - 1)}>
        decrease count -
      </button><br />
      <button onClick={() => setCountFestival(0)}>
        reset
      </button>
      </div>

      <div className="Button__Match">
      <h2>Match count</h2>
      <button onClick={() => setCountMatch((countMatch) => countMatch + 1)}>
        increase count +
      </button>
      <h2>{countMatch}</h2>
      <button onClick={() => setCountMatch((countMatch) => countMatch - 1)}>
        decrease count -
      </button><br />
      <button onClick={() => setCountMatch(0)}>
        reset
      </button>
      </div>

    </div>

  )
}