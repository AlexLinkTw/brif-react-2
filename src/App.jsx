import { useState } from 'react'
import './App.css'


function App() {

  const [count, setCount] = useState(0);

  const addCountHandler = () => {
    setCount(count + 1);
  };
  const removeCountHandler = () => {
    if(count === 0){
      return;  
    }
    setCount(count - 1);
  };

  return (
    <div className="App">
      <h1>Festival Count</h1>
      <div className="Button__Festival">
        <button onClick={addCountHandler}>+</button>
          <h2>{count}</h2>
        <button onClick={removeCountHandler}>-</button>
      </div>

    </div>
  );
};

export default App
